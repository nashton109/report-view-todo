/**
 * @todo
 */
import { Builder, Chart, IBuilderConfig, IBuilderReport } from "report-view";
export interface ITodoConfig extends IBuilderConfig {
    /** @todo */
    filename: string;
    /** @todo */
    levels?: {
        /** @todo */
        red: number;
        /** @todo */
        orange: number;
    };
}
export interface ITodoReport extends IBuilderReport {
    /** @todo */
    totals: {
        /** @todo */
        total: number;
        /** @todo */
        todo: number;
        /** @todo */
        fixme: number;
    };
    /** @todo */
    files: ITodoReportFile[];
}
export interface ITodoReportFile {
    /** @todo */
    filename: string;
    /** @todo */
    items: ITodoReportFileItem[];
}
export interface ITodoReportFileItem {
    /** @todo */
    type: string;
    /** @todo */
    line: string;
    /** @todo */
    text: string;
}
export default class Todo extends Builder {
    /** @inheritdoc */
    readonly title: string;
    /** @inheritdoc */
    readonly description: string;
    /** @inheritdoc */
    readonly path: string;
    /** @inheritdoc */
    readonly view: string;
    /** @inheritdoc */
    charts(): Promise<Chart[]>;
    /** @inheritdoc */
    protected doExecute(): Promise<ITodoReport>;
    /** @inheritdoc */
    protected getDefaultConfig(): ITodoConfig;
}
//# sourceMappingURL=Todo.d.ts.map