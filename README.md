# Reporter: TSLint

[![Todos](reports/badges/todos.svg)](#)
[![Unit Tests](reports/badges/tests.svg)](reports/tests)
[![Coverage](reports/badges/coverage.svg)](reports/coverage)

TBD

## Installation

Install using a package manager;

*Yarn*
```sh
yarn global add reporter-tslint
```

*NPM*
```sh
npm --global install reporter-tslint
```

## Usage

TBD

## Reports and Documentation

TBD
